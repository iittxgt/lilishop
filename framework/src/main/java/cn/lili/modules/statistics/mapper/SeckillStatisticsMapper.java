package cn.lili.modules.statistics.mapper;

import cn.lili.modules.promotion.entity.dos.Seckill;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Update;

/**
 * 秒杀活动统计
 *
 * @author Chopper
 * @since 2020/8/21
 */
public interface SeckillStatisticsMapper extends BaseMapper<Seckill> {
}